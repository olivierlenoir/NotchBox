// Author: Olivier Lenoir - <olivier.len02@gmail.com>
// Created: 2022-04-21
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2019.01-RC2
// Project: Raspberry Pi B+ V1.2 case, monted on 3.1mm pad
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [82, 33, 0];  // translation
$vpr = [55, 0.00, 25];  // rotation angles in degrees
$vpd = 440;  // camera distance
$vpf = undef;  // camera field of view


// include


// use
use <NotchBox.scad>;

// Main
projection(cut = false)
difference() {
// Raspberry case
    notch_box(92, 63, 30, 3, 6, 1);

    // usb
    color("green")
    translate([10.1, 75.9, -5])
    cube([8, 3, 10]);

    // hdmi
    color("green")
    translate([28, 77.5, -5])
    cube([15, 5.5, 10]);

    // jack d=6
    color("green")
    translate([57, 79.5, 0])
    cylinder(d=6, h=10, center=true);

    // sd card
    color("green")
    translate([139, 25.5, -5])
    cube([2, 12, 10]);

    // ethernet
    color("green")
    translate([105.5, 5.75, -5])
    cube([13.5, 16, 10]);

    // usb 1
    color("green")
    translate([106, 25, -5])
    cube([16, 15, 10]);

    // usb 2
    color("green")
    translate([106, 43, -5])
    cube([16, 15, 10]);
}

// Modules


// Functions

