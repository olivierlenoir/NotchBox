// Author: Olivier Lenoir - <olivier.len02@gmail.com>
// Created: 2022-04-14
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2019.01-RC2
// Project: Notch box
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [85, 37, 0];  // translation
$vpr = [55, 0.00, 25];  // rotation angles in degrees
$vpd = 435;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main

//notch_box(W, D, H, Th, F, C, Div)

// Without closed
//notch_box(55, 34, 21, 3, 6);
//projection(cut = false) notch_box(55, 34, 21, 3, 6);

//notch_box(89, 55, 34, 3, 6);
//projection(cut = false) notch_box(89, 55, 34, 3, 6);

// With closed
//notch_box(55, 34, 21, 3, 6, 1);
//projection(cut = false) notch_box(55, 34, 21, 3, 6, 1);

//notch_box(89, 55, 34, 3, 6, 1);
//projection(cut = false) notch_box(89, 55, 34, 3, 6, 1);

// With divider
//notch_box(89, 55, 34, 3, 6, Div=[5, 3]);
projection(cut = false) notch_box(89, 55, 34, 3, 6, Div=[5, 3]);

// With closed and divider
//notch_box(89, 55, 34, 3, 6, C=1, Div=[5, 3]);
//projection(cut = false) notch_box(89, 55, 34, 3, 6, C=1, Div=[5, 3]);


// Modules
module notch_box(W, D, H, Th, F, C=0, Div=[]) {
//    Outer dimensions
//    W: Width (mm)
//    D: Depth (mm)
//    H: Height (mm)
//    Th: Thickness (mm)
//    F: Finger (mm)
//    C: Closed, 0 = Not closed / 1 = Closed
//    Div: Divider, as list of number of width places and number of depth places, e.g. Div=[3, 2]

    // Finished box
//    difference() {
//        cube([W, D, H]);
//        translate ([Th, Th, Th]) cube([W - 2 * Th, D - 2 * Th, H - C * 2 * Th]);
//    }

    w_notch = notch(W, F);
    w_finger = finger(W, F);
    d_notch = notch(D, F, C=C);
    d_finger = finger(D, F, C=C);
    h_notch = notch(H, F, Th, C=C);
    h_finger = finger(H, F, Th, C=C);

    // Bottom panel
    for (n = C == 1 ? [0, 1] : [0]) {
        translate([0, -n * (D + 2 * Th), 0])
        difference() {
            cube([W, D, Th]);
            for (j = [0, D]) {
                for (i = [0:w_notch - 1]) {
                    translate([w_finger + 2 * i * w_finger, -Th + j, -Th])
                    cube([w_finger, 2 * Th, 3 * Th]);
                }
            }
            for (j = [0, W]) {
                for (i = [0:d_notch - 1]) {
                    translate([-Th + j, d_finger + 2 * i * d_finger, -Th])
                    cube([2 * Th, d_finger, 3 * Th]);
                }
            }
        };
    }

    // Width panel
    for (n = [0:1]) {
        translate([0, 2 * Th + D + n * (H + 2 * Th), 0])
        difference() {
            cube([W, H, Th]);
            for (j = C == 1 ? [0, H] : [0]) {  // If closed, for (j = [0, H])
                for (i = [0:w_notch]) {
                    offset_length = i == 0 || i == w_notch ? Th : 0;
                    offset_translate = i == 0 ? -Th : 0;
                    translate([2 * i * w_finger + offset_translate, -Th + j, -Th])
                    cube([w_finger + offset_length, 2 * Th, 3 * Th]);
                }
            }
            for (j = [0, W]) {
                for (i = [0:h_notch - 1]) {
                    translate([-Th + j, Th + h_finger + 2 * i * h_finger, -Th])
                    cube([2 * Th, h_finger, 3 * Th]);
                }
            }
        };
    }

    // Depth panel
    for (n = [0:1]) {
        translate([2 * Th + W + n * (H + 2 * Th), 0, 0])
        difference() {
            cube([H, D, Th]);
            for (j = [0, D]) {
                for (i = [0:h_notch]) {
                    offset_length = i == 0 || i == h_notch ? Th : 0;
                    offset_translate = i == 0 ? -Th : 0;
                    translate([2 * i * h_finger + Th + offset_translate, -Th + j, -Th])
                    cube([h_finger + offset_length, 2 * Th, 3 * Th]);
                }
            }
            for (j = C == 1 ? [0, H] : [0]) {  // If closed, j = [0, H]
                for (i = [0:d_notch]) {
                    offset_length = i == 0 || i == d_notch ? Th : 0;
                    offset_translate = i == 0 ? -Th : 0;
                    translate([-Th + j, 2 * i * d_finger + offset_translate, -Th])
                    cube([2 * Th, d_finger + offset_length, 3 * Th]);
                }
            }
        };
    }

    // Divider panel
    W_div = W - 2 * Th;
    D_div = D - 2 * Th;
    H_div = H - Th - C * Th;
    // width divider
    for (n = [0:Div[1] - 2]) {
        translate([0, D + 2 * H + 6 * Th + n * (H_div + 2 * Th), 0])
        difference() {
            cube([W_div, H_div, Th]);
                for (c = [1:Div[0] - 1]) {
                    translate([(c * (W - Th) / Div[0]) - Th / 2, 0, 0])
                    cube([Th, H_div, 3 * Th], center=true);
                }
        };
    }
    // depth divider
    for (n = [0:Div[0] - 2]) {
        translate([W + 2 * H + 6 * Th + n * (H_div + 2 * Th), 0, 0])
        difference() {
            cube([H_div, D_div, Th]);
                for (c = [1:Div[1] - 1]) {
                    translate([0, (c * (D - Th) / Div[1]) - Th / 2, 0])
                    cube([H_div, Th, 3 * Th], center=true);
                }
        };
    }
}


// Functions
// Number of notch based on lenght, finger size and thickness
function notch(L, F, Th=0, C=0) = floor((L - F - Th - C * Th) / (2 * F));

// Finger size
function finger(L, F, Th=0, C=0) = (L - Th - C * Th) / (notch(L, F, Th, C) * 2 + 1);
